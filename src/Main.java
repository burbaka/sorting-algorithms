public class Main {

    public static void main(String[] args) {

        System.out.println("Hello World!");


        int[] unsortedArray = {5, 0, 34, 9, 1, 56, 7, 7, 11, 1, 8, 4};

        // HeapSort sorting algorithm
        System.out.println("\nHeap sort algorithm");
        int[] sorted = HeapSort.sort(unsortedArray);
        for (int a : sorted){
            System.out.print(a + " ");
        }

        // Quick sort sorting algorithm
        System.out.println("\nQuick sort algorithm");
        sorted = QuickSort.sort(unsortedArray);
        for (int a : sorted){
            System.out.print(a + " ");
        }

        // Quick sort sorting algorithm
        System.out.println("\nMerge sort algorithm");
        sorted = MergeSort.sort(unsortedArray);
        for (int a : sorted){
            System.out.print(a + " ");
        }
    }
}
