/**
 * Implementation for QuickSort sorting algorithm
 *
 * @author Olexandr Shapovalov
 */
public class MergeSort {

    /**
     * HeapSort sorting algorithm. (Sorts ascending)
     *
     * @return Sorted array
     */
    public static int[] sort(int[] array) {

        return mergeSort(array, 0, array.length - 1);
    }

    /**
     * Merges two sorted arrays into one
     *
     * @return Result array
     */
    private static int[] mergeArrays(int[] firstArray, int[] secondArray) {
        int[] resultArray = new int[firstArray.length + secondArray.length];

        int i = 0;
        int j = 0;
        int resultIndex = 0;

        // merge two arrays until one will be empty
        while (i < firstArray.length && j < secondArray.length) {
            if (firstArray[i] < secondArray[j]) {
                resultArray[resultIndex] = firstArray[i];
                i++;
            } else {
                resultArray[resultIndex] = secondArray[j];
                j++;
            }

            resultIndex++;
        }

        // write rest of array to result array
        if (i < firstArray.length) {
            while (i < firstArray.length) {
                resultArray[resultIndex] = firstArray[i];
                i++;
                resultIndex++;
            }
        } else {
            while (j < secondArray.length) {
                resultArray[resultIndex] = secondArray[j];
                j++;
                resultIndex++;
            }

        }

        return resultArray;
    }


    public static int[] mergeSort(int[] unsortedArray, int low, int high) {
        int middle =  (high + low) / 2;

        if (low == high) {
            int[] singleItemArray = new int[1];
            singleItemArray[0] = unsortedArray[middle];
            return singleItemArray;
        }

        int[] firstHalfSorted = mergeSort(unsortedArray, low, middle);
        int[] secondHalfSorted = mergeSort(unsortedArray, middle + 1, high);
        return mergeArrays(firstHalfSorted, secondHalfSorted);
    }
}
