/**
 * Implementation for QuickSort sorting algorithm
 *
 * @author Olexandr Shapovalov
 */
public class QuickSort {

    /**
     * HeapSort sorting algorithm. (Sorts ascending)
     *
     * @return Sorted array
     */
    public static int[] sort(int[] array) {
        qSort(array, 0, array.length - 1);

        return array;
    }

    private static void qSort(int[] unsortedArray, int inputLeftIndex, int inputRightIndex) {
        int leftIndex = inputLeftIndex;
        int rightIndex = inputRightIndex;

        int x = unsortedArray[(leftIndex + rightIndex) / 2];

        while (leftIndex <= rightIndex) {
            while (unsortedArray[leftIndex] < x) {
                leftIndex++;
            }
            while (unsortedArray[rightIndex] > x) {
                rightIndex--;
            }

            if (leftIndex <= rightIndex) {
                swap(unsortedArray, leftIndex, rightIndex);
                leftIndex++;
                rightIndex--;
            }
        }
        if (rightIndex > inputLeftIndex) {
            qSort(unsortedArray, inputLeftIndex, rightIndex);
        }
        if (leftIndex < inputRightIndex) {
            qSort(unsortedArray, leftIndex, inputRightIndex);
        }
    }

    /**
     * Swap elements in the array
     */
    private static void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
