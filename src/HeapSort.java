/**
 * Implementation for HeapSort sorting algorithm
 * @author Olexandr Shapovalov
*/
public class HeapSort {

    /**
     * Change heap by swapping nodes in tree to meet the following rule: every node is bigger then it's 2 sub nodes (children)
     */
    private static void heapify(int[] array, int size, int pos) {
        int subNode = 2 * pos + 1;

        // sub heap normalization process
        while (subNode < size) {

            // check if right sub node exist and is bigger then left one
            if (subNode + 1 < size && array[subNode] < array[subNode + 1]) {
                subNode = 2 * pos + 2;  // set right node to be subNode
            }

            if (array[pos] < array[subNode]) {
                swap(array, pos, subNode);
                pos = subNode;
                subNode = 2 * pos + 1;
            } else {
                break;
            }
        }
    }


    /**
     * Build heap from input array.
     */
    private static int[] heapMake(int[] array) {
        int n = array.length;
        for (int i = n - 1; i >= 0; i--) {
            heapify(array, n, i);
        }
        return array;
    }

    /**
     * HeapSort sorting algorithm. (Sorts ascending)
     * @return Sorted array
     */
    public static int[] sort(int[] array) {
        heapMake(array); // make general tree based heap from array

        for (int heapSize = array.length - 1; heapSize > 0; heapSize--){
            swap(array, 0, heapSize);
            heapify(array, heapSize, 0);
        }

        return array;
    }

    /**
     * Swap elements in the array
     */
    private static void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

}
